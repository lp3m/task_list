import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'user-login.dart';
import 'user-register.dart';
import 'task-create.dart';
import 'task-list.dart';

const firebaseOptions = FirebaseOptions(
  apiKey: "AIzaSyBHdbbDooy2SI5XrAaIpZaUf-IsqtpeOJg",
  projectId: "task-list-caa66",
  appId: "1:872354356232:web:529f9fb5e7194668d485bd",
  authDomain: "task-list-caa66.firebaseapp.com",
  storageBucket: "task-list-caa66.appspot.com",
  messagingSenderId: "872354356232",
);

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: firebaseOptions);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // home: TaskListPage(),
      initialRoute: '/user-login',
      routes: {
        '/task-list': (context) => TaskListPage(), 
        '/task-create':(context) => TaskCreatePage(),
        '/user-register':(context) => UserRegisterPage(),
        '/user-login':(context) => UserLoginPage(),
      }
    );
  }
}