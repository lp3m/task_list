import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'task.dart';

class TaskListPage extends StatelessWidget {
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  FirebaseAuth auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Task List"),
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: firestore
          .collection('tasks')
          .where('uid', isEqualTo: auth.currentUser!.uid)
          .snapshots(),
        builder: (context, snapshot) {

          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }

          var tasks = snapshot.data!.docs
              as List<QueryDocumentSnapshot<Map<String, dynamic>>>;

          return ListView(
            children: tasks.map((t) => Dismissible(
                    key: Key(t.id),
                    onDismissed: (_) => firestore.collection('tasks').doc(t.id).delete(),
                    background: Container(color: Colors.red),
                    child: CheckboxListTile(
                      title: Text(t['name']),
                      value: t['finished'],
                      onChanged: (value) => firestore.collection('tasks')
                        .doc(t.id).update({'finished': value})
                    ),
                  ),
                ).toList(),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.of(context).pushNamed('/task-create'),
        child: Icon(Icons.add),
      ),
    );
  }
}